# remote state
remote_state_bucket = "sky-back-end"

# EC2 variables for production
ec2_instance_type = "t2.micro"
ec2_min_instance_size = 2
ec2_max_instance_size = 6
