output "webapp-loadbalancer-dns" {
  value = aws_elb.webapp-load-balancer.dns_name
}

output "webapp-instance-ips" {
  value = data.aws_instances.web-app-production-instances.public_ips
}

