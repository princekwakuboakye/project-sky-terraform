# vpc_and_ec2_architecture

Reference architecture for a highly-available, scalable, fault-tolerant and resilient infrastructure on AWS with VPC and EC2

INFRASTRUCTURE
This folder contains all VPC
To bring the environment up you have to give the below commands.

This is to provision the backend for the infrastructure.
$ terraform init -backend-config="infrastructure-prod.config"

This is to plan the infrastructure you want to create, picking the production.tvars values.
$ terraform plan -var-file="infra-production.tfvars"

This is to apply the codes on AWS, to see you infrastructure in action.
$ terraform apply -var-file="infra-production.tfvars"

this is to destroy the infrastructure
$ terraform destroy -var-file="infra-production.tfvars"


PLATFORM
This is where the all the platform services are located.
The first step is to provsion the backend
$ terraform init -backend-config="backend-prod.config"

To provision the services apply the blow commands
$ terraform plan -var-file="platform-production.tfvars"

$ terraform apply -var-file="platform-production.tfvars"

$ terraform destroy -var-file="platform-production.tfvars"



DEPLOYMENT

Install Apache using Ansible Playbook

1. Login to Ansible machine. Create SSH keys in Ansible host machine by executing the below command:

ssh-keygen
2. Copy the public keys(id_rsa.pub) from ansible node each node in /home/ubuntu/.ssh/authorized_keys file. Execute the below command on Ansible and copy the content:
 sudo cat ~/.ssh/id_rsa.pub

3. Now login to target node, execute the below command to open the file
sudo vi /home/ubuntu/.ssh/authorized_keys

4. go back to ansible mgmt node, make sure you are able to ssh from ansible mgmt node after copying the keys above:
ssh private_ip_of_target_node
now type exit to come out of the target node.

5. Now in ansible mgmt node, now make changes in /etc/ansible/hosts file to include the node you will be installing software. Make sure you add public IP address TO (XX.XX.XX.XX)
sudo vi /etc/ansible/hosts
[Apache_Group]  
XX.XX.XX.XX ansible_ssh_user=ubuntu ansible_ssh_private_key_file=~/.ssh/id_rsa  ansible_python_interpreter=/usr/bin/python3

6. make changes in playbooks as given below,
cd ~/playbooks

7. now execute the playbook by running the below command:
sudo ansible-playbook installApache.xml
8. This should install Apache on the nodes and should bring the apache up and running.
9. Now enter public ip address or public dns name of target server by in the browser to see home page of Apache running.

